const axios = require('../services/axios');

module.exports = async (apiUrl, recordPerPage, criteria) => {
  const {
    data: {
      status,
      data = [],
    },
  } = await axios.get(apiUrl, {
    params: {
      start: 1,
      limit: recordPerPage,
      ...criteria,
    },
  });

  // console.error()
  const results = [];
  // set dev for reducing small usage on api credit
  const totalRecord = status?.total_count;
  let responseResult = data;

  if (totalRecord > recordPerPage) {
    for (let i = recordPerPage; i < totalRecord; i += recordPerPage) {
      // loop until get all record
      results.push(axios.get(apiUrl, {
        params: {
          start: i + 1,
          limit: recordPerPage,
          ...criteria,
        },
      }));
    }

    const promiseAsyncRecords = await Promise.all(results);
    const arrRecords = promiseAsyncRecords.map((e) => e?.data?.data || []);
    responseResult = responseResult.concat(...arrRecords);
  }

  return responseResult;
};
