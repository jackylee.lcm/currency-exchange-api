module.exports = (err, res) => {
  const statusCode = err.code || 500;
  res.status(statusCode).json({
    code: statusCode,
    statusText: err.statusText || 'Error',
    message: err.message,
  });
};
