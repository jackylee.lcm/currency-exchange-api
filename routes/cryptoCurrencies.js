const express = require('express');
const controllers = require('../controllers/cryptoCurrencies');
const paramValidate = require('../middlewares/paramValidate');

const router = express.Router();

router.route('/topMoving').get(paramValidate.topMovingCurrency, controllers.getTopMovingCrypto);

module.exports = router;
