# Currency Exchange API

An example to create RESTful API using NodeJS which retrieve top moving of crypto currency and convert the foreign currency for each crypto currency.

All data is refer to **[CoinMarketCap](https://coinmarketcap.com/api/documentation/v1/#section/Introduction "CoinMarketCap")**.

**Library**
- AJV - using for parameter validation
- timsort - sort data with high efficiency
- jest - unit test

## Environment/Version
- OS: ubuntu
- npm: 6.14.16
- node: 14.17.0

## Getting started

1. Get the project
```
cd existing_repo
git clone xxxxx
git pull
yarn install
```

2. Create .env.local file for environment variable
```
COINMARKETCAP_API_KEY=xxxxx
COINMARKETCAP_API_URL=https://pro-api.coinmarketcap.com/v1
```

3. Run in Dev
```
yarn start
```

4. Try API 
(Please input the higher value for reduce number of record due to CoinMarketCap API credit usage.)
```
http://xxxx.xxxx.xxxx/cryptoCurrencies/topMoving?changeRatio=100&convert=JPY
```
**changeRatio** (Integer) - Filter top moving of crypto currency
**convert** (String) - Convert the foreign currency

## Testing
**Jest** is using for unit test. Test file locate in the tests directory. Unit Test is triggered by CI/CD.
```
yarn test
```
## Deployment
A simple CI/CD flow is available on gitlab of this project. Deployment will be triggered after you commit code to gitlab.
