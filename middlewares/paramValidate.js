const Ajv = require('ajv');

const ErrorCfg = require('../config/error');
const resFormatHandler = require('../utils/resFormatHandler');

const ajv = new Ajv({ coerceTypes: true });

module.exports = {
  topMovingCurrency: (req, res, next) => {
    const schema = {
      type: 'object',
      properties: {
        changeRatio: { type: 'integer' },
        convert: { type: 'string' },
      },
    };

    const validate = ajv.compile(schema);
    const valid = validate(req.query);
    if (!valid) {
      const errorArr = validate.errors.map((e) => `${e.instancePath.replace('/', '')} ${e.message}`);
      resFormatHandler({
        ...ErrorCfg.validateParam,
        message: errorArr.join(' & '),
      }, res);
    } else {
      next();
    }
  },
};
