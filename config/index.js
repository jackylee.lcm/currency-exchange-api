const config = {
  coinMarketCapAPIKey: process.env.COINMARKETCAP_API_KEY,
  coinMarketCapAPIUrl: process.env.COINMARKETCAP_API_URL,
};
module.exports = config;
