require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const resFormatHandler = require('./utils/resFormatHandler');

const app = express();

const corsOptions = {
  origin: process.env?.CORS?.split(',') || 'http://localhost:3000',
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/cryptoCurrencies', require('./routes/cryptoCurrencies'));

app.use((err, req, res, next) => resFormatHandler(err, res));

// set port, listen for requests
const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

module.exports = app;
