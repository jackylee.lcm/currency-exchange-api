const axios = require('../services/axios');

describe('services - axios', () => {
  test('connect to api', async () => {
    try {
      await axios.get('/test');
    } catch (err) {
      expect(err.response.status).toBe(404);
    }
  });
});
