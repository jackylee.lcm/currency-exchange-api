const axios = require('../services/axios');

const paginationLoop = require('../utils/getAllAPIData');

jest.mock('../services/axios');

describe('utils - getAllAPIData', () => {
  test('4 records per page (total records 10)', async () => {
    const dataStatus = {
      total_count: 10,
    };
    const limit = 4;

    axios.get.mockResolvedValueOnce({ data: { status: dataStatus, data: [1, 2, 3, 4] } });
    axios.get.mockResolvedValueOnce({ data: { status: dataStatus, data: [5, 6, 7, 8] } });
    axios.get.mockResolvedValueOnce({ data: { status: dataStatus, data: [9, 10] } });

    const apiUrl = '';
    const result = await paginationLoop(apiUrl, limit);
    expect(result).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
  });

  // test('test 60 per page (total records ~100)', async () => {
  //   const apiUrl = '/cryptocurrency/listings/latest';
  //   const limit = 60;
  //   // market_cap_min=800000000, total record should be around 100
  //   const result = await paginationLoop(apiUrl, limit, {
  //     market_cap_min: 800000000,
  //   });
  //   expect(result.length).toBeGreaterThanOrEqual(60);
  // });
});
