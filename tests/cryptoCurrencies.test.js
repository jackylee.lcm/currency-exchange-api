const cryptoCurrencies = require('../controllers/cryptoCurrencies');
// const axios = require('../services/axios');

// jest.mock('../services/axios');

const next = jest.fn();

const mockResponse = () => {
  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res;
};

// const res = {
//   status: jest.fn(
//     (status) => ({
//       json: jest.fn((resObj) => ({ res: { ...resObj, statusCode: status } })),
//     }),
//   ),
// };

describe('controllers - cryptoCurrencies', () => {
  test('getTopMovingCrypto - changeRatio 100, should 200 and return correct value', async () => {
    const res = mockResponse();
    const changeRatio = '100';
    await cryptoCurrencies.getTopMovingCrypto({ query: { changeRatio, convert: 'JPY' } }, res, next);
    expect(res.status).toHaveBeenCalledWith(200);
    if (res.json.mock.calls[0][0].responseData.length > 0) {
      const mockCallData = res.json.mock.calls[0][0].responseData;
      expect(mockCallData).not.toBeUndefined();

      const covertCurrency = mockCallData[0].quote?.JPY;
      expect(covertCurrency).not.toBeUndefined();

      const percentChange24h = mockCallData[0].quote?.JPY.percent_change_24h;
      if (percentChange24h >= 100) { expect(percentChange24h).toBeGreaterThanOrEqual(100); }
      if (percentChange24h <= -100) { expect(percentChange24h).toBeLessThanOrEqual(-100); }
    }
  });
});
