const paramValidate = require('../middlewares/paramValidate');

const next = jest.fn();
const res = {
  status: jest.fn(
    (status) => ({
      json: jest.fn((resObj) => ({ res: { ...resObj, statusCode: status } })),
    }),
  ),
};

describe('middleware - paramValidate', () => {
  test('success - topMovingCurrency', () => {
    paramValidate.topMovingCurrency({ query: { changeRatio: '5', convert: 'USD' } }, res, next);
    expect(next).toHaveBeenCalled();
  });
  test('fail - topMovingCurrency', () => {
    paramValidate.topMovingCurrency({ query: { changeRatio: 'tv', convert: 'USD' } }, res, next);
    expect(res.status).toHaveBeenCalled();
  });
});
