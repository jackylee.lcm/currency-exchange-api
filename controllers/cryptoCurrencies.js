const TimSort = require('timsort');
// const axios = require('../services/axios');
const paginationLoop = require('../utils/getAllAPIData');

exports.getTopMovingCrypto = async (req, res, next) => {
  try {
    // Plan credit use: 1 call credit per 200 cryptocurrencies returned (rounded up)
    // set limit to 200 for testing and prevent higher credit usage
    const apiUrl = '/cryptocurrency/listings/latest';

    // coinMarketApi max limit
    const maxLimit = 5000;

    const { changeRatio = 5, convert = 'USD' } = req.query;

    const increaseDataResult = await paginationLoop(apiUrl, maxLimit, {
      percent_change_24h_min: changeRatio,
      convert,
    });
    const decreaseDataResult = await paginationLoop(apiUrl, maxLimit, {
      percent_change_24h_max: +changeRatio * -1,
      convert,
    });

    const responseData = increaseDataResult.concat(decreaseDataResult);

    // timSort for high efficient on sorting
    TimSort.sort(
      responseData,
      (a, b) => (b?.quote?.[convert]?.market_cap || 0) - (a?.quote?.[convert]?.market_cap || 0),
    );

    // success
    res.status(200).json({
      status: 'success',
      length: responseData?.length,
      responseData,
    });
  } catch (err) {
    const {
      status,
      statusText,
      data,
    } = err.response;
    next({
      code: status,
      statusText,
      message: data?.status?.error_message || '',
    });
  }
};
