const axios = require('axios');
const config = require('../config');

const instance = axios.create({
  baseURL: config.coinMarketCapAPIUrl,
  headers: {
    'X-CMC_PRO_API_KEY': config.coinMarketCapAPIKey,
  },
});

module.exports = instance;
